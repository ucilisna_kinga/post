package com.jovsal.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;

@Service
public class JwtFilterUtil {

  private String header = "Authorization";
  private String secret = "mySecret";
  private String expirationHours = "120";

  public JwtFilterUtil(String header, String secret, String expirationHours) {
    this.header = header;
    this.secret = secret;
    this.expirationHours = expirationHours;
  }

  public JwtFilterUtil() {
  }

  public String getRelativePath (String absolutePath) {
    String pathWithoutHttp = absolutePath.split("//")[1];
    String[] pathWithoutHttpSplitedParts = pathWithoutHttp.split("/");

    if (pathWithoutHttpSplitedParts.length == 1) {
      return "/";
    } else {
      String[] partsRemovedFirst = Arrays.copyOfRange(pathWithoutHttpSplitedParts, 1, pathWithoutHttpSplitedParts.length);
      return String.join("/", partsRemovedFirst);
    }
  }

  public Boolean isAllowedPath (String path) {
    if(path.equals("/") ||
      path.startsWith("auth") ||
      path.startsWith("file") ||
      path.equals("favicon.ico") ||
      path.startsWith("insertPostsData") ||
      path.startsWith("test"))
    {
      return true;
    } else {
      return false;
    }
  }

  public Boolean isTokenValidAndFresh(String token) {
    try {
      if(!isTokenExpired(token)) {
        return true;
      } else {
        return false;
      }
    } catch ( Exception e ) {
      return false;
    }
  }

  private Boolean isTokenExpired(String token) {
    final Date expiration = getExpirationDateFromToken(token);
    return expiration.before(new Date());
  }

  private Date getExpirationDateFromToken(String token) {
    Date expiration;
    try {
      final Claims claims = getClaimsFromToken(token);
      expiration = claims.getExpiration();
    } catch (Exception e) {
      expiration = null;
    }
    return expiration;
  }

  public Claims getClaimsFromToken(String token) {
    Claims claims;
    try {
      claims = Jwts.parser()
        .setSigningKey(secret)
        .parseClaimsJws(token)
        .getBody();
    } catch (Exception e) {
      claims = null;
    }
    return claims;
  }

  public String getHeader() {
    return header;
  }

  public void setHeader(String header) {
    this.header = header;
  }

  public String getSecret() {
    return secret;
  }

  public void setSecret(String secret) {
    this.secret = secret;
  }

  public String getExpirationHours() {
    return expirationHours;
  }

  public void setExpirationHours(String expirationHours) {
    this.expirationHours = expirationHours;
  }

  public String getUsernameFromClaims(String header) {
    Claims claimsFromToken = getClaimsFromToken(header);
    return claimsFromToken.get("username").toString();
  }
}
