package com.jovsal.security.jsonHelpers;

public class TokenDto extends JsonResult {
  private String token;

  public TokenDto(String token) {
    this.token = token;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
}
