package com.jovsal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
//public class PostApplication implements CommandLineRunner {
public class PostApplication {

//	@Resource
//	StorageService storageService;

  public static void main(String[] args) {
		SpringApplication.run(PostApplication.class, args);
	}

//	@Override
//	public void run(String... arg) throws Exception {
////		storageService.deleteAll();
//		storageService.init();
//	}
}
