package com.jovsal.post;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;



@Service
public class StorageService {

    Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final Path rootLocation = Paths.get("/upload-dir");
    // dev only windows
//    private final Path rootLocation = Paths.get("C:\\upload-dir");

    public String store(MultipartFile file) {
        try {
            String newFileName = UUID.randomUUID().toString();
            if(file.getOriginalFilename().endsWith("png")){
                newFileName+=".png";
            } else if (file.getOriginalFilename().endsWith("jpg")) {
                newFileName+=".jpg";
            } else if (file.getOriginalFilename().endsWith("jpeg")) {
                newFileName+=".jpeg";
            }
            Files.copy(file.getInputStream(), this.rootLocation.resolve(newFileName));
            return newFileName;
        } catch (Exception e) {
            throw new RuntimeException("FAIL!");
        }
    }

    public Resource loadFile(String filename) {
        try {
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("FAIL!");
        }
    }

    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    public void init() {
        if(!rootLocation.toFile().exists()) {
            try {
                Files.createDirectory(rootLocation);
            } catch (IOException e) {
                throw new RuntimeException("Could not initialize storage!");
            }
        }

    }
}