package com.jovsal.post;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "post")
public class Post {

  @Id private String id;
  @Column private String text;
  @Column private String createdByUsername;
  @Column private String timeCreated;
  @Column private String picture;
  @ElementCollection private List<String> likes;

  public Post() {}

  public Post(String id, String text, String createdByUsername, String timeCreated, String picture, List<String> likes) {
    this.id = id;
    this.text = text;
    this.createdByUsername = createdByUsername;
    this.timeCreated = timeCreated;
    this.picture = picture;
    this.likes = likes;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getCreatedByUsername() {
    return createdByUsername;
  }

  public void setCreatedByUsername(String createdByUsername) {
    this.createdByUsername = createdByUsername;
  }

  public String getTimeCreated() {
    return timeCreated;
  }

  public void setTimeCreated(String timeCreated) {
    this.timeCreated = timeCreated;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public List<String> getLikes() {
    return likes;
  }

  public void setLikes(List<String> likes) {
    this.likes = likes;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Post post = (Post) o;

    if (id != null ? !id.equals(post.id) : post.id != null) return false;
    if (text != null ? !text.equals(post.text) : post.text != null) return false;
    if (createdByUsername != null ? !createdByUsername.equals(post.createdByUsername) : post.createdByUsername != null)
      return false;
    if (timeCreated != null ? !timeCreated.equals(post.timeCreated) : post.timeCreated != null) return false;
    if (picture != null ? !picture.equals(post.picture) : post.picture != null) return false;
    return likes != null ? likes.equals(post.likes) : post.likes == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (text != null ? text.hashCode() : 0);
    result = 31 * result + (createdByUsername != null ? createdByUsername.hashCode() : 0);
    result = 31 * result + (timeCreated != null ? timeCreated.hashCode() : 0);
    result = 31 * result + (picture != null ? picture.hashCode() : 0);
    result = 31 * result + (likes != null ? likes.hashCode() : 0);
    return result;
  }
}
